import Gomoku
import System.IO

-- | If we are crosses, we just choose the middle of the field as it is the best position.
-- Then we can proceed with a game loop as if our opponent started first.
startGame :: Symbol -> GameField -> IO ()
startGame mySymbol field =
    if mySymbol == X then do
        let myCoords = (matrixWidth field `div` 2, matrixWidth field `div` 2)
        putStrLn (showCoords myCoords)
        let field' = placeSymbol myCoords mySymbol field
        -- printGameField field'
        loop mySymbol field'
    else
        loop mySymbol field

-- | The game loop. First, it reads the oponent’s turn, then it responds with its own choice of coordinates.
-- This is repeated indefinitely.
loop :: Symbol -> GameField -> IO ()
loop mySymbol = loop'
    where
        loop' field = do
            coords <- readCoords <$> getLine
            let field' = placeSymbol coords (otherSymbol mySymbol) field
            -- printGameField field'
            let myCoords = computeCoords coords field'
            putStrLn (showCoords myCoords)
            let field'' = placeSymbol myCoords mySymbol field'
            -- printGameField field''
            loop' field''

-- | Reads the size of the field and our symbol. After that it starts the game.
main :: IO ()
main = do
    hSetBuffering stdin LineBuffering
    hSetBuffering stdout LineBuffering
    size <- read <$> getLine
    mySymbol <- readSymbol <$> getLine
    let field = emptyGameField size
    startGame mySymbol field
