{-# LANGUAGE OverloadedStrings #-}

import Data.Monoid ((<>))
import Data.Text (Text)
import qualified Data.Text as T
import Gomoku
import qualified Network.WebSockets as WS

-- * Helpers

showAsText :: Show s => s -> Text
showAsText = T.pack . show

coordsToJson :: Coords -> Text
coordsToJson (x, y) = "{\"x\": " <> showAsText (x - 1)  <> ", \"y\":" <> showAsText (y - 1)  <> "}"

-- | Converts a text to JSON encoded string. Let’s hope this won’t break.
textToJson :: Text -> Text
textToJson text = "\"" <> escapeQuotes text  <> "\""
    where
        escapeQuotes = T.replace "\"" "\\\""

readTextCoords :: Text -> Coords
readTextCoords = readCoords . T.unpack

readInitMsg :: Text -> (Int, Symbol)
readInitMsg msg =
    let
        [size, symbol] = T.words msg
    in
        (read (T.unpack size), readSymbol (T.unpack symbol))

-- * Application

main :: IO ()
main = WS.runServer "0.0.0.0" 9160 application

application :: WS.ServerApp
application pending = do
    conn <- WS.acceptRequest pending
    WS.forkPingThread conn 30
    (size, symbol) <- readInitMsg <$> WS.receiveData conn
    let field = emptyGameField size
    startGame symbol field conn

startGame :: Symbol -> GameField -> WS.Connection -> IO ()
startGame mySymbol field conn =
    if mySymbol == X then do
        let myCoords = (matrixWidth field `div` 2, matrixWidth field `div` 2)
        WS.sendTextData conn (coordsToJson myCoords)
        let field' = placeSymbol myCoords mySymbol field
        loop 2 mySymbol field' conn
    else
        loop 1 mySymbol field conn

-- | The game loop. First, it reads the oponent’s turn, then it responds with its own choice of coordinates.
-- This is repeated indefinitely.
loop :: Int -> Symbol -> GameField -> WS.Connection -> IO ()
loop initialStep mySymbol fld conn = loop' initialStep fld
    where
        maxSteps = matrixWidth fld ^ 2
        loop' step field = do
            coords <- readTextCoords <$> WS.receiveData conn
            let field' = placeSymbol coords (otherSymbol mySymbol) field
            if hasGameEnded coords field' then
                WS.sendTextData conn (textToJson "You win!")
            else if step == maxSteps then
                WS.sendTextData conn (textToJson "Tie")
            else do
                let myCoords = computeCoords coords field'
                let field'' = placeSymbol myCoords mySymbol field'
                WS.sendTextData conn (coordsToJson myCoords)
                if hasGameEnded myCoords field'' then
                    WS.sendTextData conn (textToJson "You lose!")
                else if step + 1 == maxSteps then
                    WS.sendTextData conn (textToJson "Tie")
                else
                    loop' (step + 2) field''
