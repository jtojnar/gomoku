Compile with:

```
ghc --make -O2 Cli.hs
```

On platforms that do not support ANSI escape sequences (Windows), it is possible to turn them off by compiling with `-DNOCOLOURS` option.
