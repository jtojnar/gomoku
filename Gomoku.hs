{-# LANGUAGE CPP #-}

module Gomoku where

import Data.Array
import Data.List
import Data.Maybe
import Data.Ord

-- * Type definitions

type Coords = (Int, Int)

-- | Matrix is represented by an Array indexed by pairs of numbers (coordinates).
-- The coordinates start with @(1, 1)@ and end with @(width, height)@.
type Matrix val = Array Coords val

type GameFieldCell = Maybe Symbol

type GameField = Matrix GameFieldCell

data Symbol = X | O
    deriving (Show, Eq)

-- | Orientation of a line.
data Orientation
    = Dash -- ^ (@—@) is horizontal
    | Pipe -- ^ (@|@) is vertical
    | ForwardSlash -- ^ (@/@) is diagonal oriented northeast–southwest
    | BackwardSlash -- ^ (@\\@) is diagonal oriented southeast–northwest
    deriving (Show, Eq)


-- * Basic helpers

matrixDims :: Matrix a -> Coords
matrixDims = snd . bounds

matrixWidth :: Matrix a -> Int
matrixWidth = fst . matrixDims

matrixHeight :: Matrix a -> Int
matrixHeight = snd . matrixDims

isInsideMatrixBounds :: Matrix a -> Coords -> Bool
isInsideMatrixBounds m (x, y) = xOk && yOk
    where
        xOk = x >= 1 && x <= matrixWidth m
        yOk = y >= 1 && y <= matrixHeight m

addSextuple :: (Num t₁, Num t₂, Num t₃, Num t₄, Num t₅, Num t₆) => (t₁, t₂, t₃, t₄, t₅, t₆) -> (t₁, t₂, t₃, t₄, t₅, t₆) -> (t₁, t₂, t₃, t₄, t₅, t₆)
addSextuple (x₁, x₂, x₃, x₄, x₅, x₆) (y₁, y₂, y₃, y₄, y₅, y₆) = (x₁ + y₁, x₂ + y₂, x₃ + y₃, x₄ + y₄, x₅ + y₅, x₆ + y₆)

-- | Strips the longest prefix, some of whose elements match the predicate, up to the given length.
--
-- > stripWhileUpTo odd 3 [1, 2, 3, 4, 5] == [4, 5]
stripWhileUpTo :: (a -> Bool) -> Int -> [a] -> [a]
stripWhileUpTo p m list = dr m list list
    where
        dr 0 _ ret = ret
        dr _n [] ret = ret
        dr n (x : xs) ret = dr (n - 1) xs (if p x then xs else ret)

-- | Strips the longest prefix and the longest suffix, some of whose elements match the predicate, up to the given length.
stripWhileUpToLR :: (a -> Bool) -> Int -> [a] -> [a]
stripWhileUpToLR p m xs = reverse (stripWhileUpTo p m (reverse (stripWhileUpTo p m xs)))

dropAround :: (a -> Bool) -> [a] -> [a]
dropAround p = dropWhileEnd p . dropWhile p

otherSymbol :: Symbol -> Symbol
otherSymbol X = O
otherSymbol O = X

allOrientations :: [Orientation]
allOrientations = [Dash, Pipe, ForwardSlash, BackwardSlash]


-- * Input/Output helpers

visualiseGameFieldCell :: GameFieldCell -> Coords -> String
visualiseGameFieldCell (Just X) = backLightGameFieldCell "X"
visualiseGameFieldCell (Just O) = backLightGameFieldCell "O"
visualiseGameFieldCell Nothing = backLightGameFieldCell " "

-- | Provides a grid for easier reasoning about the game field.
-- ANSI escape sequences are used so it will not work on terminals that do not support them.
-- It can by disabled by compiling with @-DNOCOLOUR@.
backLightGameFieldCell :: String -> Coords -> String
#ifndef NOCOLOUR
backLightGameFieldCell symbol (x, y) = colour ++ symbol ++ reset
    where
        colour = if (x + y) `mod` 2 == 1 then "\ESC[30;47m" else "\ESC[37;40m"
        reset = "\ESC[0m"
#else
backLightGameFieldCell symbol _coords = symbol
#endif

visualiseGameField :: GameField -> String
visualiseGameField field = unlines [concat [visualiseGameFieldCell (field ! (x, y)) (x, y) | x <- [1..matrixWidth field]] | y <- [1..matrixHeight field]]

printGameField :: GameField -> IO ()
printGameField = putStrLn . visualiseGameField

-- | Splits the string and returns tuple of the two numbers.
-- As the coordinates on input are zero indexed, we have to convert them.
readCoords :: String -> Coords
readCoords = (\[x, y] -> (x + 1, y + 1)) . map read . words

readSymbol :: String -> Symbol
readSymbol "X" = X
readSymbol "O" = O
readSymbol _ = error "Cannot read field."

-- | Converts coordinates to zero-indexed form expected by the competition system.
showCoords :: Coords -> String
showCoords (x, y) = show (x - 1) ++ " " ++ show (y - 1)

printCoords :: Coords -> IO ()
printCoords = putStrLn . showCoords


-- * Game field helpers

emptyGameField :: Int -> GameField
emptyGameField size = listArray ((1, 1), (size, size)) (replicate (size * size) Nothing)

placeSymbol :: Coords -> Symbol -> GameField -> GameField
placeSymbol coords symbol field =
    if isJust (field ! coords) then
        error ("The field " ++ show coords ++ " is already occupied.")
    else
        field // [(coords, Just symbol)]

listEmptyCoords :: GameField -> [Coords]
listEmptyCoords = map fst . filter (isNothing . snd) . assocs

listOccupiedCoords :: GameField -> [Coords]
listOccupiedCoords = map fst . filter (isJust . snd) . assocs


-- * Decision strategy helpers

-- | Returns envelope of points. That means all the points that can potentially create
-- a winning sequence with the given points.
surroundings :: Int -> [Coords] -> [Coords]
surroundings = undefined

coordsListBoundingBox :: [Coords] -> (Coords, Coords)
coordsListBoundingBox coordsList = ((minimum xs, minimum ys), (maximum xs, maximum ys))
    where
        xs = map fst coordsList
        ys = map snd coordsList

-- | Returns minimal rectangle needed to fit the original bounding rectangle and the point.
extendBoundingBox :: (Coords, Coords) -> Coords -> (Coords, Coords)
extendBoundingBox ((x1, y1), (x2, y2)) (x3, y3) = ((x1', y1'), (x2', y2'))
    where
        x1' = if x3 < x1 then x3 else x1
        y1' = if y3 < y1 then y3 else y1
        x2' = if x3 > x2 then x3 else x2
        y2' = if y3 > y2 then y3 else y2

inside :: (Coords, Coords) -> Coords -> Bool
inside boundingBox point = extendBoundingBox boundingBox point == boundingBox

-- | Clip list of coordinates to fit a game field. As the points form a line, only the points
-- on the start and the end of the list need to be dropped.
clipLinePoints :: GameField -> [Coords] -> [Coords]
clipLinePoints field coordsList = dropAround (not . isInsideMatrixBounds field) coordsList

-- | Transforms list of coordinates to the list of contents of the cells.
materialiseLine :: Coords -> Symbol -> GameField -> [Coords] -> [GameFieldCell]
materialiseLine baseCoords expectedSymbol field coordsList = map (\coords -> if coords == baseCoords then Just expectedSymbol else field ! coords) coordsList

linePointFromOrigin :: Orientation -> Coords -> Int -> Coords
linePointFromOrigin Dash (x, y) off = (x + off, y)
linePointFromOrigin Pipe (x, y) off  = (x, y + off)
linePointFromOrigin BackwardSlash (x, y) off = (x + off, y + off)
linePointFromOrigin ForwardSlash (x, y) off = (x - off, y + off)

-- | Returns line of sight for given cell and orientation. I.e. until reaching four cells or
-- coming upon opponent’s cell, in each direction of the orientation.
getRelevantCells :: Orientation -> Coords -> Symbol -> GameField -> [GameFieldCell]
getRelevantCells orientation coords expectedSymbol field =
    let
        current = Just expectedSymbol
        opponent = otherSymbol <$> current
        linePoints = [lp | off <- [-4..4], let lp = linePointFromOrigin orientation coords off]
        clippedCoords = clipLinePoints field linePoints
        cells = materialiseLine coords expectedSymbol field clippedCoords
    in
        stripWhileUpToLR (== opponent) 4 cells

-- | Counts the number of 0-turn wins, 1-turn wins, 2-turn wins and 4-turn wins; that means
-- the number of possible placements of zero, one, two or three symbols, resulting in a victory.
countGapsForCellList :: [GameFieldCell] -> (Int, Int, Int, Int, Int, Int)
countGapsForCellList cells = conseq True (map isJust cells) (0, 0, 0, 0, 0, 0)
    where
        -- base cases
        conseq _ [_, _, _, _] res = res
        conseq _ [_, _, _] res = res
        conseq _ [_, _] res = res
        conseq _ [_] res = res
        conseq _ [] res = res
        -- five consecutive
        conseq _ (True : True : True : True : True : xs) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives + 1, fours, foursBlocked, threes, threesBlocked, twos)
        -- one turn needed for five consecutive, but blocked on the right
        conseq _ (False : xs@[True, True, True, True]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked + 1, threes, threesBlocked, twos)
        conseq _ (True : xs@[False, True, True, True]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked + 1, threes, threesBlocked, twos)
        conseq _ (True : xs@[True, False, True, True]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked + 1, threes, threesBlocked, twos)
        conseq _ (True : xs@[True, True, False, True]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked + 1, threes, threesBlocked, twos)
        conseq _ (True : xs@[True, True, True, False]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked + 1, threes, threesBlocked, twos)
        -- one turn needed for five consecutive, but blocked on the left
        conseq True (False : xs@(True : True : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked + 1, threes, threesBlocked, twos)
        conseq True (True : xs@(False : True : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked + 1, threes, threesBlocked, twos)
        conseq True (True : xs@(True : False : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked + 1, threes, threesBlocked, twos)
        conseq True (True : xs@(True : True : False : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked + 1, threes, threesBlocked, twos)
        conseq True (True : xs@(True : True : True : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked + 1, threes, threesBlocked, twos)
        -- one turn needed for five consecutive
        conseq False (False : xs@(True : True : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours + 1, foursBlocked, threes, threesBlocked, twos)
        conseq False (True : xs@(False : True : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours + 1, foursBlocked, threes, threesBlocked, twos)
        conseq False (True : xs@(True : False : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours + 1, foursBlocked, threes, threesBlocked, twos)
        conseq False (True : xs@(True : True : False : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours + 1, foursBlocked, threes, threesBlocked, twos)
        conseq False (True : xs@(True : True : True : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours + 1, foursBlocked, threes, threesBlocked, twos)
        -- two turns needed for five consecutive, but blocked on the right
        conseq _ (False : xs@[False, True, True, True]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq _ (False : xs@[True, False, True, True]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq _ (False : xs@[True, True, False, True]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq _ (False : xs@[True, True, True, False]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq _ (True : xs@[False, False, True, True]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq _ (True : xs@[False, True, False, True]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq _ (True : xs@[False, True, True, False]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq _ (True : xs@[True, False, False, True]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq _ (True : xs@[True, False, True, False]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq _ (True : xs@[True, True, False, False]) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        -- two turns needed for five consecutive, but blocked on the left
        conseq True (False : xs@(False : True : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq True (False : xs@(True : False : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq True (False : xs@(True : True : False : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq True (False : xs@(True : True : True : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq True (True : xs@(False : False : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq True (True : xs@(False : True : False : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq True (True : xs@(False : True : True : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq True (True : xs@(True : False : False : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq True (True : xs@(True : False : True : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        conseq True (True : xs@(True : True : False : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked + 1, twos)
        -- two turns needed for five consecutive
        conseq _ (False : xs@(False : True : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes + 1, threesBlocked, twos)
        conseq _ (False : xs@(True : False : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes + 1, threesBlocked, twos)
        conseq _ (False : xs@(True : True : False : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes + 1, threesBlocked, twos)
        conseq _ (False : xs@(True : True : True : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes + 1, threesBlocked, twos)
        conseq _ (True : xs@(False : False : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes + 1, threesBlocked, twos)
        conseq _ (True : xs@(False : True : False : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes + 1, threesBlocked, twos)
        conseq _ (True : xs@(False : True : True : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes + 1, threesBlocked, twos)
        conseq _ (True : xs@(True : False : False : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes + 1, threesBlocked, twos)
        conseq _ (True : xs@(True : False : True : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes + 1, threesBlocked, twos)
        conseq _ (True : xs@(True : True : False : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes + 1, threesBlocked, twos)
        -- three turns needed for five consecutive
        conseq _ (True : True : xs@(False : False : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked, twos + 1)
        conseq _ (True : xs@(False : True : False : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked, twos + 1)
        conseq _ (True : xs@(False : False : True : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked, twos + 1)
        conseq _ (True : xs@(False : False : False : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked, twos + 1)
        conseq _ (False : xs@(True : True : False : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked, twos + 1)
        conseq _ (False : xs@(True : False : True : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked, twos + 1)
        conseq _ (False : xs@(True : False : False : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked, twos + 1)
        conseq _ (False : xs@(False : True : True : False : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked, twos + 1)
        conseq _ (False : xs@(False : True : False : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked, twos + 1)
        conseq _ (False : xs@(False : False : True : True : _)) (fives, fours, foursBlocked, threes, threesBlocked, twos) =
            conseq False xs (fives, fours, foursBlocked, threes, threesBlocked, twos + 1)
        -- more than three turns needed for five consecutive
        conseq _ (_ : xs) res = conseq False xs res

countGapsForOrientation :: Coords -> Symbol -> GameField -> Orientation -> (Int, Int, Int, Int, Int, Int)
countGapsForOrientation coords expectedSymbol field orientation =
    let
        cells = getRelevantCells orientation coords expectedSymbol field
    in
        countGapsForCellList cells

countGapsAroundCell :: GameField -> Coords -> Symbol -> (Int, Int, Int, Int, Int, Int)
countGapsAroundCell field coords expectedSymbol =
    let
        theirRankedOrientations = map (countGapsForOrientation coords expectedSymbol field) allOrientations
    in
        foldr addSextuple (0, 0, 0, 0, 0, 0) theirRankedOrientations

-- | Decides whether the cell on given coordinates ended the game.
hasGameEnded :: Coords -> GameField -> Bool
hasGameEnded coords field =
    let
        (fives, _fours, _foursBlocked, _threes, _threesBlocked, _twos) = countGapsAroundCell field coords (fromJust (field ! coords))
    in
        fives > 0

countEmptyAround :: Coords -> Symbol -> GameField -> Int
countEmptyAround (x, y) expectedSymbol field =
    let
        surroundingCoords = filter (isInsideMatrixBounds field) [(x + xOff, y + yOff) | xOff <- [-5..5], yOff <- [-5..5]]
        surroundingCells = map (\coords -> if (x, y) == coords then Just expectedSymbol else field ! coords) surroundingCoords
    in
        length (filter isNothing surroundingCells)

-- | Ranks cell based on seven different criteria, each more important than the next.
-- If we can win in one turn, we will. When there is no such turn but we can still prevent
-- the opponent winning in one turn, we will. We alternate between trying to build five in a row
-- and preventing the opponent build the same, while increasing the acceptable gaps.
-- Last we check for number of empty cells in the immediate surroundings of the cell,
-- to discourage the placement of the symbols to corners.
rankCell :: GameField -> Coords -> Symbol -> (Int, Int, Int, Int, Int, Int, Int, Int)
rankCell field coords mySymbol =
    let
        (ourFives, ourFours, ourFoursBlocked, ourThrees, ourThreesBlocked, ourTwos) = countGapsAroundCell field coords mySymbol
        (theirFives, theirFours, _theirFoursBlocked, _theirThrees, _theirThreesBlocked, _theirTwos) = countGapsAroundCell field coords (otherSymbol mySymbol)
        emptySurroundings = countEmptyAround coords mySymbol field
    in
        (ourFives, theirFives, ourFours, theirFours, ourThrees, ourTwos, ourFoursBlocked + ourThreesBlocked, emptySurroundings)


-- * Actual game strategy

-- | Chooses the best place to place a symbol, hopefully.
computeCoords :: Coords -> GameField -> Coords
computeCoords lastCoords field =
    let
        mySymbol = otherSymbol (fromJust (field ! lastCoords))
        emptyCoords = listEmptyCoords field
        rankedPlacements = map (\coords -> (coords, rankCell field coords mySymbol)) emptyCoords
    in
        fst (maximumBy (comparing snd) rankedPlacements)
