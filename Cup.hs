import Data.List
import Data.List.Split
import Data.Maybe
import Gomoku
import System.IO

cupShowCoords :: Coords -> String
cupShowCoords (x, y) = show (x - 1) ++ "," ++ show (y - 1)

readCupCoords :: String -> Coords
readCupCoords coords =
    let
        [x, y] = splitOn "," coords
    in
        (read x + 1, read y + 1)

getLineOrCommand :: IO String
getLineOrCommand = do
    ln <- getLine
    withFile "protocol.log" AppendMode (flip hPutStrLn ln)
    if ln == "ABOUT" then
        putStrLn "name=\"Gomoku.hs\", version=\"1.0\", author=\"sst\", country=\"CZE\"" >>  getLineOrCommand
    else if isPrefixOf "INFO" ln then
        getLineOrCommand
    else
        return ln

-- | If we receive @BEGIN@ command, we are crosses and begin so we just choose
-- the middle of the field as it is the best position.
-- Then we can proceed with a game loop as if our opponent started first.
-- If we do receive something else than a @BEGIN@ command, we pass it to game loop as it is probably a @TURN@ command.
startGame :: GameField -> IO ()
startGame field = do
    ln <- getLineOrCommand
    if ln == "BEGIN" then do
        let mySymbol = X
        let myCoords = (matrixWidth field `div` 2, matrixWidth field `div` 2)
        putStrLn (cupShowCoords myCoords)
        let field' = placeSymbol myCoords mySymbol field
        loop mySymbol "" field'
    else
        loop O ln field

-- | The game loop. First, it reads the oponent’s turn, then it responds with its own choice of coordinates.
-- This is repeated indefinitely.
loop :: Symbol -> String -> GameField -> IO ()
loop mySymbol = loop'
    where
        loop' initTurn field = do
            coords <- readCupCoords . fromJust . stripPrefix "TURN " <$> if null initTurn then getLineOrCommand else return initTurn
            let field' = placeSymbol coords (otherSymbol mySymbol) field
            let myCoords = computeCoords coords field'
            putStrLn (cupShowCoords myCoords)
            let field'' = placeSymbol myCoords mySymbol field'
            loop' "" field''

-- | Reads the size of the field. After that it starts the game.
main :: IO ()
main = do
    hSetBuffering stdin LineBuffering
    hSetBuffering stdout LineBuffering
    size <- read . fromJust . stripPrefix "START " <$> getLineOrCommand
    putStrLn "OK"
    let field = emptyGameField size
    startGame field
